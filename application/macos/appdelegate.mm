#import "appdelegate.h"
#import "pagecurlview.h"
#import <Cocoa/Cocoa.h>

@implementation AppDelegate
{
  NSWindow* _window;
  PageCurlView* _view;
}

-(id) init
{
    if (self = [super init])
    {
        PageCurlParticleGrid particleGrid(24, 24);
        CGRect contentSize = NSMakeRect(0, 0, 800, 400);
        NSUInteger windowStyleMask = NSWindowStyleMaskTitled |
                                     NSWindowStyleMaskClosable |
                                     NSWindowStyleMaskMiniaturizable |
                                     NSWindowStyleMaskResizable |
                                     NSWindowStyleMaskTexturedBackground;

        _window = [[NSWindow alloc] initWithContentRect:contentSize
                                     styleMask:windowStyleMask
                                     backing:NSBackingStoreBuffered
                                     defer:YES];
        _window.appearance = [NSAppearance appearanceNamed:NSAppearanceNameVibrantDark];
        _window.contentAspectRatio = NSMakeSize(2, 1);
        _window.minSize = NSMakeSize(300, 150);
        _window.title = @"PageCurlᴹᵀᴸ";

        _view = [[PageCurlView alloc] initWithParticleGrid:particleGrid frameRect:contentSize];
    }
    return self;
}

-(void) dealloc
{
    [_window release];
    [_view release];
    [super dealloc];
}

-(void)applicationWillFinishLaunching:(NSNotification*)pNotification
{
    [[_window standardWindowButton:NSWindowZoomButton] setEnabled:NO];
    [_window setContentView:_view];
    [_window center];
}

-(void)applicationDidFinishLaunching:(NSNotification*)pNotification
{
    [_window makeKeyAndOrderFront:self];
}

-(BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication*)pSender
{
    return YES;
}

@end

