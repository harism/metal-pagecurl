#import "pagecurlrenderer.h"
#import "pagecurlutils.h"

#define RENDER_POINTS_AND_DRAG_INFO FALSE

@implementation PageCurlRenderer
{
    BOOL _isInitialized;

    id<MTLTexture> _imageTexture;

    id<MTLBuffer> _triangleBuffer;
    id<MTLComputePipelineState> _initializeTrianglesPipeline;

    id<MTLBuffer> _renderTrianglesFragArgumentBuffer;
    id<MTLBuffer> _renderTrianglesVertArgumentBuffer;
    id<MTLRenderPipelineState> _renderTrianglesPipeline;

    id<MTLBuffer> _renderPointsArgumentBuffer;
    id<MTLRenderPipelineState> _renderPointsPipeline;

    id<MTLRenderPipelineState> _renderMoveLinePipeline;
    id<MTLRenderPipelineState> _renderMovePositionPipeline;
}

-(instancetype) initWithView:(MTKView*)view
                library:(id<MTLLibrary>)library
                constants:(const PageCurlConstants&)constants
                constantsBuffer:(id<MTLBuffer>)constantsBuffer
                particleBuffer:(id<MTLBuffer>)particleBuffer
{
    if (self = [super init])
    {
        _isInitialized = NO;
        auto device = [library device];

        NSDictionary<MTKTextureLoaderOption, id> *textureLoadOptions = @{
            MTKTextureLoaderOptionOrigin : MTKTextureLoaderOriginBottomLeft
        };

        auto pagecurlBundle = [NSBundle mainBundle];
        auto pagecurlImageUrl = [pagecurlBundle URLForResource:@"image" withExtension:@"jpg"];
        auto textureLoader = [[MTKTextureLoader alloc] initWithDevice:device];
        _imageTexture = [textureLoader newTextureWithContentsOfURL:pagecurlImageUrl
                                       options:textureLoadOptions
                                       error:nil];
        [textureLoader release];
        [pagecurlImageUrl release];
        [pagecurlBundle release];

        _triangleBuffer = [device newBufferWithLength:(sizeof(PageCurlTriangle) * constants.triangleCount)
                                         options:MTLResourceStorageModePrivate];
        auto initializeTrianglesFunction = [library newFunctionWithName:@"InitializeTriangle"];
        auto initializeTrianglesDescriptor = [[MTLComputePipelineDescriptor alloc] init];
        [initializeTrianglesDescriptor setLabel:@"Initialize triangles pipeline"];
        [initializeTrianglesDescriptor setComputeFunction:initializeTrianglesFunction];
        _initializeTrianglesPipeline = [device newComputePipelineStateWithDescriptor:initializeTrianglesDescriptor
                                               options:MTLPipelineOptionNone
                                               reflection:nil
                                               error:nil];
        [initializeTrianglesFunction release];
        [initializeTrianglesDescriptor release];

        auto renderTriangleVertexFunction = [library newFunctionWithName:@"RenderTriangleVertex"];
        auto renderTriangleFragmentFunction = [library newFunctionWithName:@"RenderTriangleFragment"];
        _renderTrianglesVertArgumentBuffer = [device newBufferWithLength:sizeof(PageCurlArgumentBuffer)
                                                     options:MTLResourceStorageModeShared];
        auto renderTrianglesVertArgumentEncoder = [renderTriangleVertexFunction newArgumentEncoderWithBufferIndex:0];
        [renderTrianglesVertArgumentEncoder setArgumentBuffer:_renderTrianglesVertArgumentBuffer
                                            offset:0];
        [renderTrianglesVertArgumentEncoder setBuffer:particleBuffer
                                            offset:0
                                            atIndex:PAGECURL_ARGUMENT_PARTICLES];
        [renderTrianglesVertArgumentEncoder setBuffer:_triangleBuffer
                                            offset:0
                                            atIndex:PAGECURL_ARGUMENT_TRIANGLES];
        [renderTrianglesVertArgumentEncoder setBuffer:constantsBuffer
                                            offset:0
                                            atIndex:PAGECURL_ARGUMENT_CONSTANTS];
        _renderTrianglesFragArgumentBuffer = [device newBufferWithLength:sizeof(PageCurlArgumentBuffer)
                                                     options:MTLResourceStorageModeShared];
        auto renderTrianglesFragArgumentEncoder = [renderTriangleFragmentFunction newArgumentEncoderWithBufferIndex:0];
        [renderTrianglesFragArgumentEncoder setArgumentBuffer:_renderTrianglesFragArgumentBuffer
                                            offset:0];
        [renderTrianglesFragArgumentEncoder setTexture:_imageTexture atIndex:PAGECURL_ARGUMENT_IMAGE];
        auto renderTrianglesPipelineDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
        [renderTrianglesPipelineDescriptor setLabel:@"Render triangles pipeline"];
        [renderTrianglesPipelineDescriptor setVertexFunction:renderTriangleVertexFunction];
        [renderTrianglesPipelineDescriptor setFragmentFunction:renderTriangleFragmentFunction];
        [[renderTrianglesPipelineDescriptor colorAttachments][0] setPixelFormat:view.colorPixelFormat];
        _renderTrianglesPipeline = [device newRenderPipelineStateWithDescriptor:renderTrianglesPipelineDescriptor
                                           error:nil];
        [renderTriangleVertexFunction release];
        [renderTriangleFragmentFunction release];
        [renderTrianglesVertArgumentEncoder release];
        [renderTrianglesFragArgumentEncoder release];
        [renderTrianglesPipelineDescriptor release];

        auto renderPointVertexFunction = [library newFunctionWithName:@"RenderPointVertex"];
        auto renderPointFragmentFunction = [library newFunctionWithName:@"RenderPointFragment"];
        _renderPointsArgumentBuffer = [device newBufferWithLength:sizeof(PageCurlArgumentBuffer)
                                              options:MTLResourceStorageModeShared];
        auto renderPointsArgumentEncoder = [renderPointVertexFunction newArgumentEncoderWithBufferIndex:0];
        [renderPointsArgumentEncoder setArgumentBuffer:_renderPointsArgumentBuffer
                                     offset:0];
        [renderPointsArgumentEncoder setBuffer:particleBuffer
                                     offset:0
                                     atIndex:PAGECURL_ARGUMENT_PARTICLES];
        [renderPointsArgumentEncoder setBuffer:constantsBuffer
                                     offset:0
                                     atIndex:PAGECURL_ARGUMENT_CONSTANTS];
        auto renderPointsPipelineDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
        [renderPointsPipelineDescriptor setLabel:@"RenderPoint Pipeline"];
        [renderPointsPipelineDescriptor setVertexFunction:renderPointVertexFunction];
        [renderPointsPipelineDescriptor setFragmentFunction:renderPointFragmentFunction];
        [[renderPointsPipelineDescriptor colorAttachments][0] setPixelFormat:view.colorPixelFormat];
        _renderPointsPipeline = [device newRenderPipelineStateWithDescriptor:renderPointsPipelineDescriptor
                                        error:nil];
        [renderPointVertexFunction release];
        [renderPointFragmentFunction release];
        [renderPointsArgumentEncoder release];
        [renderPointsPipelineDescriptor release];

        auto renderMoveLineVertexFunction = [library newFunctionWithName:@"RenderMoveLineVertex"];
        auto renderMoveLineFragmentFunction = [library newFunctionWithName:@"RenderMoveLineFragment"];
        auto renderMoveLinePipelineDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
        [renderMoveLinePipelineDescriptor setLabel:@"RenderMoveLine Pipeline"];
        [renderMoveLinePipelineDescriptor setVertexFunction:renderMoveLineVertexFunction];
        [renderMoveLinePipelineDescriptor setFragmentFunction:renderMoveLineFragmentFunction];
        [[renderMoveLinePipelineDescriptor colorAttachments][0] setPixelFormat:view.colorPixelFormat];
        _renderMoveLinePipeline = [device newRenderPipelineStateWithDescriptor:renderMoveLinePipelineDescriptor
                                          error:nil];
        [renderMoveLineVertexFunction release];
        [renderMoveLineFragmentFunction release];
        [renderMoveLinePipelineDescriptor release];

        auto renderMovePositionVertexFunction = [library newFunctionWithName:@"RenderMovePositionVertex"];
        auto renderMovePositionFragmentFunction = [library newFunctionWithName:@"RenderMovePositionFragment"];
        auto renderMovePositionPipelineDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
        [renderMovePositionPipelineDescriptor setLabel:@"RenderMovePosition Pipeline"];
        [renderMovePositionPipelineDescriptor setVertexFunction:renderMovePositionVertexFunction];
        [renderMovePositionPipelineDescriptor setFragmentFunction:renderMovePositionFragmentFunction];
        [[renderMovePositionPipelineDescriptor colorAttachments][0] setPixelFormat:view.colorPixelFormat];
        _renderMovePositionPipeline = [device newRenderPipelineStateWithDescriptor:renderMovePositionPipelineDescriptor
                                              error:nil];
        [renderMovePositionVertexFunction release];
        [renderMovePositionFragmentFunction release];
        [renderMovePositionPipelineDescriptor release];
    }
    return self;
}

-(void) render:(nonnull MTKView*)view
        commandQueue:(id<MTLCommandQueue>)commandQueue
        constants:(const PageCurlConstants&)constants
{
    id<MTLCommandBuffer> commandBuffer = [commandQueue commandBuffer];
    [commandBuffer setLabel:@"Render command buffer"];

    if (_isInitialized == NO)
    {
        _isInitialized = YES;
        const auto threadsSize = MTLSizeMake(constants.particleGrid.x - 1,
                                             constants.particleGrid.y - 1,
                                             2);
        const auto threadgroupSize = [PageCurlUtils threadgroupSize:_initializeTrianglesPipeline
                                                    threadsSize:threadsSize];
        auto initializeTrianglesEncoder = [commandBuffer computeCommandEncoder];
        [initializeTrianglesEncoder setLabel:@"Initialize triangles encoder"];
        [initializeTrianglesEncoder setComputePipelineState:_initializeTrianglesPipeline];
        [initializeTrianglesEncoder setBuffer:_triangleBuffer
                                    offset:0
                                    atIndex:0];
        [initializeTrianglesEncoder setBytes:&constants
                                    length:sizeof(PageCurlConstants)
                                    atIndex:1];
        [initializeTrianglesEncoder dispatchThreads:threadsSize
                                    threadsPerThreadgroup:threadgroupSize];
        [initializeTrianglesEncoder endEncoding];
    }

    if (auto renderPassDescriptor = [view currentRenderPassDescriptor])
    {
        CGSize viewportSize = [view drawableSize];
        auto viewport = (MTLViewport) { 0.0, 0.0, viewportSize.width, viewportSize.height, -1.0, 1.0 };

        auto mainEncoder = [commandBuffer parallelRenderCommandEncoderWithDescriptor:renderPassDescriptor];

        auto renderTrianglesEncoder = [mainEncoder renderCommandEncoder];
        [renderTrianglesEncoder setLabel:@"Render triangles encoder"];
        [renderTrianglesEncoder setViewport:viewport];
        [renderTrianglesEncoder setRenderPipelineState:_renderTrianglesPipeline];
        [renderTrianglesEncoder setVertexBuffer:_renderTrianglesVertArgumentBuffer
                                offset:0
                                atIndex:0];
        [renderTrianglesEncoder setFragmentBuffer:_renderTrianglesFragArgumentBuffer
                                offset:0
                                atIndex:0];
        [renderTrianglesEncoder setVertexBytes:&constants.projectionMatrix
                                length:sizeof(matrix_float4x4)
                                atIndex:1];
        [renderTrianglesEncoder drawPrimitives:MTLPrimitiveTypeTriangle
                                vertexStart:0
                                vertexCount:3
                                instanceCount:constants.triangleCount
                                baseInstance:0];
        [renderTrianglesEncoder endEncoding];

#if RENDER_POINTS_AND_DRAG_INFO
        auto renderPointsEncoder = [mainEncoder renderCommandEncoder];
        [renderPointsEncoder setLabel:@"Render points encoder"];
        [renderPointsEncoder setViewport:viewport];
        [renderPointsEncoder setRenderPipelineState:_renderPointsPipeline];
        [renderPointsEncoder setVertexBuffer:_renderPointsArgumentBuffer
                             offset:0
                             atIndex:0];
        [renderPointsEncoder setVertexBytes:&constants.projectionMatrix
                             length:sizeof(matrix_float4x4)
                             atIndex:1];
        [renderPointsEncoder drawPrimitives:MTLPrimitiveTypePoint
                             vertexStart:0
                             vertexCount:constants.particleCount];
        [renderPointsEncoder endEncoding];

        if (constants.moveInfo.state == PAGECURL_MOVESTATE_INIT ||
            constants.moveInfo.state == PAGECURL_MOVESTATE_MOVE)
        {
            auto renderMoveLineEncoder = [mainEncoder renderCommandEncoder];
            [renderMoveLineEncoder setLabel:@"Render move line encoder"];
            [renderMoveLineEncoder setViewport:viewport];
            [renderMoveLineEncoder setRenderPipelineState:_renderMoveLinePipeline];
            [renderMoveLineEncoder setVertexBytes:&constants
                                   length:sizeof(PageCurlConstants)
                                   atIndex:0];
            [renderMoveLineEncoder drawPrimitives:MTLPrimitiveTypeLine
                                   vertexStart:0
                                   vertexCount:2];
            [renderMoveLineEncoder endEncoding];

            auto renderMovePositionEncoder = [mainEncoder renderCommandEncoder];
            [renderMovePositionEncoder setLabel:@"Render move position encoder"];
            [renderMovePositionEncoder setViewport:viewport];
            [renderMovePositionEncoder setRenderPipelineState:_renderMovePositionPipeline];
            [renderMovePositionEncoder setVertexBytes:&constants
                                       length:sizeof(PageCurlConstants)
                                       atIndex:0];
            [renderMovePositionEncoder drawPrimitives:MTLPrimitiveTypePoint
                                       vertexStart:0
                                       vertexCount:1];
            [renderMovePositionEncoder endEncoding];
        }
#endif // RENDER_POINTS_AND_DRAG_INFO

        [mainEncoder endEncoding];
        [commandBuffer presentDrawable:view.currentDrawable];
    }

    [commandBuffer commit];
}

@end

