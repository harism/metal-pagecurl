#import "pagecurlutils.h"

@implementation PageCurlUtils

+(MTLSize) threadgroupSize:(id<MTLComputePipelineState>)computePipeline
           threadsSize:(const MTLSize&)threadsSize
{
    const auto maxWidth = [computePipeline threadExecutionWidth];
    const auto maxHeight = [computePipeline maxTotalThreadsPerThreadgroup] / maxWidth;
    const auto threadgroupSize = MTLSizeMake(MIN(maxWidth, threadsSize.width),
                                             MIN(maxHeight, threadsSize.height),
                                             threadsSize.depth);
    return threadgroupSize;
}

@end
