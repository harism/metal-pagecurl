#import "pagecurlsimulation.h"
#import "pagecurlutils.h"

@implementation PageCurlSimulation
{
    BOOL _initialize;
    id<MTLBuffer> _particleBuffer;

    id<MTLBuffer> _initializeParticlesArgumentBuffer;
    id<MTLComputePipelineState> _initializeParticlesPipeline;

    id<MTLBuffer> _advanceParticlesArgumentBuffer;
    id<MTLComputePipelineState> _advanceParticlesPipeline;

    id<MTLBuffer> _constrainParticlesArgumentBuffer;
    id<MTLComputePipelineState> _constrainParticlesPipeline;

    id<MTLBuffer> _calcParticleNormalsArgumentBuffer;
    id<MTLComputePipelineState> _calcParticleNormalsPipeline;
}

-(instancetype) initWithLibrary:(id<MTLLibrary>)library
                constants:(const PageCurlConstants&)constants
                constantsBuffer:(id<MTLBuffer>)constantsBuffer
{
    if (self = [super init])
    {
        auto device = [library device];

        _initialize = YES;
        _particleBuffer = [device newBufferWithLength:(constants.particleCount * sizeof(PageCurlParticle))
                                  options:MTLResourceStorageModePrivate];

        auto initializeParticlesFunction = [library newFunctionWithName:@"InitializeParticle"];
        _initializeParticlesArgumentBuffer = [device newBufferWithLength:sizeof(PageCurlArgumentBuffer)
                                                     options:MTLResourceStorageModeShared];
        auto initializeParticlesArgumentEncoder = [initializeParticlesFunction newArgumentEncoderWithBufferIndex:0];
        [initializeParticlesArgumentEncoder setArgumentBuffer:_initializeParticlesArgumentBuffer offset:0];
        [initializeParticlesArgumentEncoder setBuffer:_particleBuffer offset:0 atIndex:PAGECURL_ARGUMENT_PARTICLES];
        [initializeParticlesArgumentEncoder setBuffer:constantsBuffer offset:0 atIndex:PAGECURL_ARGUMENT_CONSTANTS];
        auto initializeParticlesDescriptor = [[MTLComputePipelineDescriptor alloc] init];
        [initializeParticlesDescriptor setLabel:@"Initialize particles pipeline"];
        [initializeParticlesDescriptor setComputeFunction:initializeParticlesFunction];
        _initializeParticlesPipeline = [device newComputePipelineStateWithDescriptor:initializeParticlesDescriptor
                                               options:MTLPipelineOptionNone
                                               reflection:nil
                                               error:nil];
        [initializeParticlesFunction release];
        [initializeParticlesArgumentEncoder release];
        [initializeParticlesDescriptor release];

        auto advanceParticlesFunction = [library newFunctionWithName:@"AdvanceParticle"];
        _advanceParticlesArgumentBuffer = [device newBufferWithLength:sizeof(PageCurlArgumentBuffer)
                                                  options:MTLResourceStorageModeShared];
        auto advanceParticlesArgumentEncoder = [advanceParticlesFunction newArgumentEncoderWithBufferIndex:0];
        [advanceParticlesArgumentEncoder setArgumentBuffer:_advanceParticlesArgumentBuffer offset:0];
        [advanceParticlesArgumentEncoder setBuffer:_particleBuffer offset:0 atIndex:PAGECURL_ARGUMENT_PARTICLES];
        [advanceParticlesArgumentEncoder setBuffer:constantsBuffer offset:0 atIndex:PAGECURL_ARGUMENT_CONSTANTS];
        auto advanceParticlesDescriptor = [[MTLComputePipelineDescriptor alloc] init];
        [advanceParticlesDescriptor setLabel:@"Advance particles pipeline"];
        [advanceParticlesDescriptor setComputeFunction:advanceParticlesFunction];
        _advanceParticlesPipeline = [device newComputePipelineStateWithDescriptor:advanceParticlesDescriptor
                                            options:MTLPipelineOptionNone
                                            reflection:nil
                                            error:nil];
        [advanceParticlesFunction release];
        [advanceParticlesArgumentEncoder release];
        [advanceParticlesDescriptor release];

        auto constrainParticlesFunction = [library newFunctionWithName:@"ConstrainParticle"];
        _constrainParticlesArgumentBuffer = [device newBufferWithLength:sizeof(PageCurlArgumentBuffer)
                                                    options:MTLResourceStorageModeShared];
        auto constrainParticlesArgumentEncoder = [constrainParticlesFunction newArgumentEncoderWithBufferIndex:0];
        [constrainParticlesArgumentEncoder setArgumentBuffer:_constrainParticlesArgumentBuffer offset:0];
        [constrainParticlesArgumentEncoder setBuffer:_particleBuffer offset:0 atIndex:PAGECURL_ARGUMENT_PARTICLES];
        [constrainParticlesArgumentEncoder setBuffer:constantsBuffer offset:0 atIndex:PAGECURL_ARGUMENT_CONSTANTS];
        auto constrainParticlesDescriptor = [[MTLComputePipelineDescriptor alloc] init];
        [constrainParticlesDescriptor setLabel:@"Constrain particles pipeline"];
        [constrainParticlesDescriptor setComputeFunction:constrainParticlesFunction];
        _constrainParticlesPipeline = [device newComputePipelineStateWithDescriptor:constrainParticlesDescriptor
                                              options:MTLPipelineOptionNone
                                              reflection:nil
                                              error:nil];
        [constrainParticlesFunction release];
        [constrainParticlesArgumentEncoder release];
        [constrainParticlesDescriptor release];

        auto calcParticleNormalsFunction = [library newFunctionWithName:@"CalcParticleNormal"];
        _calcParticleNormalsArgumentBuffer = [device newBufferWithLength:sizeof(PageCurlArgumentBuffer)
                                                     options:MTLResourceStorageModeShared];
        auto calcParticleNormalsArgumentEncoder = [calcParticleNormalsFunction newArgumentEncoderWithBufferIndex:0];
        [calcParticleNormalsArgumentEncoder setArgumentBuffer:_calcParticleNormalsArgumentBuffer offset:0];
        [calcParticleNormalsArgumentEncoder setBuffer:_particleBuffer offset:0 atIndex:PAGECURL_ARGUMENT_PARTICLES];
        [calcParticleNormalsArgumentEncoder setBuffer:constantsBuffer offset:0 atIndex:PAGECURL_ARGUMENT_CONSTANTS];
        auto calcParticleNormalsDescriptor = [[MTLComputePipelineDescriptor alloc] init];
        [calcParticleNormalsDescriptor setLabel:@"Calculate particle normals pipeline"];
        [calcParticleNormalsDescriptor setComputeFunction:calcParticleNormalsFunction];
        _calcParticleNormalsPipeline = [device newComputePipelineStateWithDescriptor:calcParticleNormalsDescriptor
                                               options:MTLPipelineOptionNone
                                               reflection:nil
                                               error:nil];
        [calcParticleNormalsFunction release];
        [calcParticleNormalsArgumentEncoder release];
        [calcParticleNormalsDescriptor release];
    }
    return self;
}

-(void)dealloc
{
    [_initializeParticlesArgumentBuffer release];
    [_initializeParticlesPipeline release];
    [_advanceParticlesArgumentBuffer release];
    [_advanceParticlesPipeline release];
    [_calcParticleNormalsArgumentBuffer release];
    [_calcParticleNormalsPipeline release];
    [_particleBuffer release];
    [super dealloc];
}

-(id<MTLBuffer>) particleBuffer
{
    return _particleBuffer;
}

-(void) simulate:(id<MTLCommandQueue>)commandQueue
        constants:(const PageCurlConstants &)constants
{
    id<MTLCommandBuffer> commandBuffer = [commandQueue commandBuffer];
    [commandBuffer setLabel:@"Simulation command buffer"];

    if (_initialize == YES)
    {
        _initialize = NO;
        const auto threadsSize = MTLSizeMake(constants.particleGrid.x,
                                             constants.particleGrid.y,
                                             1);
        const auto threadgroupSize = [PageCurlUtils threadgroupSize:_initializeParticlesPipeline
                                                    threadsSize:threadsSize];
        auto encoder = [commandBuffer computeCommandEncoder];
        [encoder setLabel:@"Initialize particles encoder"];
        [encoder setComputePipelineState:_initializeParticlesPipeline];
        [encoder setBuffer:_initializeParticlesArgumentBuffer offset:0 atIndex:0];
        [encoder dispatchThreads:threadsSize threadsPerThreadgroup:threadgroupSize];
        [encoder endEncoding];
    }

    if (constants.moveInfo.state != PAGECURL_MOVESTATE_NONE)
    {
        const auto maxWidth = [_constrainParticlesPipeline threadExecutionWidth];
        const auto maxHeight = [_constrainParticlesPipeline maxTotalThreadsPerThreadgroup] / maxWidth;
        auto encoder = [commandBuffer computeCommandEncoder];
        [encoder setLabel:@"Advance particles encoder"];
        [encoder setComputePipelineState:_advanceParticlesPipeline];
        [encoder setBuffer:_advanceParticlesArgumentBuffer offset:0 atIndex:0];
        [encoder dispatchThreadgroups:MTLSizeMake((constants.particleGrid.x + maxWidth - 1) / maxWidth,
                                                  (constants.particleGrid.y + maxHeight - 1) / maxHeight,
                                                  1)
                 threadsPerThreadgroup:MTLSizeMake(maxWidth, maxHeight, 1)];
        [encoder endEncoding];
    }

    if (constants.moveInfo.state != PAGECURL_MOVESTATE_NONE)
    {
        const auto maxWidth = [_constrainParticlesPipeline threadExecutionWidth];
        const auto maxHeight = [_constrainParticlesPipeline maxTotalThreadsPerThreadgroup] / maxWidth;
        for (int iterate = 0; iterate < 8; ++iterate)
        {
            auto encoder = [commandBuffer computeCommandEncoder];
            [encoder setLabel:@"Constrain particles encoder"];
            [encoder setComputePipelineState:_constrainParticlesPipeline];
            [encoder setBuffer:_constrainParticlesArgumentBuffer offset:0 atIndex:0];
            [encoder dispatchThreadgroups:MTLSizeMake(1, 1, 1)
                     threadsPerThreadgroup:MTLSizeMake(maxWidth, maxHeight, 1)];
            [encoder endEncoding];
        }
    }

    {
        const auto threadsSize = MTLSizeMake(constants.particleGrid.x,
                                             constants.particleGrid.y,
                                             1);
        const auto threadgroupSize = [PageCurlUtils threadgroupSize:_calcParticleNormalsPipeline
                                                    threadsSize:threadsSize];
        auto encoder = [commandBuffer computeCommandEncoder];
        [encoder setLabel:@"Calculate particle normals encoder"];
        [encoder setComputePipelineState:_calcParticleNormalsPipeline];
        [encoder setBuffer:_calcParticleNormalsArgumentBuffer offset:0 atIndex:0];
        [encoder dispatchThreads:threadsSize threadsPerThreadgroup:threadgroupSize];
        [encoder endEncoding];
    }

    [commandBuffer commit];
}

@end
