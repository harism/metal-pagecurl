#import <MetalKit/MetalKit.h>
#import "pagecurltypes.h"

@interface PageCurlSimulation : NSObject

-(nullable instancetype) initWithLibrary:(nonnull id<MTLLibrary>)library
                         constants:(const PageCurlConstants&)constants
                         constantsBuffer:(nonnull id<MTLBuffer>)constantsBuffer;

-(nullable id<MTLBuffer>) particleBuffer;

-(void) simulate:(nonnull id<MTLCommandQueue>)commandQueue
        constants:(const PageCurlConstants&)constants;

@end
