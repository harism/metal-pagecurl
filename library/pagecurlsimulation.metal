#import <metal_stdlib>
#import <simd/simd.h>
#import "pagecurltypes.h"

using namespace metal;

kernel void InitializeParticle(constant PageCurlArgumentBuffer& arguments [[buffer(0)]],
                               uint3 gid [[thread_position_in_grid]])
{
    device PageCurlConstants& constants = *(arguments.constants);
    device PageCurlParticle* particles = arguments.particles;

    if (gid.x >= constants.particleGrid.x ||
        gid.y >= constants.particleGrid.y ||
        gid.z >= 1)
    {
        return;
    }

    const uint index = (gid.y * constants.particleGrid.x) + gid.x;
    const float3 position = float3(mix( 0.0, 1.0, (float)gid.x / (constants.particleGrid.x - 1)),
                                   mix(-1.0, 1.0, (float)gid.y / (constants.particleGrid.y - 1)),
                                   0);
    particles[index].position = position;
    particles[index].positionPrev = position;
    particles[index].texturePosition = float2(position.x, position.y * 0.5 + 0.5);
    particles[index].restDistance = 0.0;
}

kernel void AdvanceParticle(constant PageCurlArgumentBuffer& arguments [[buffer(0)]],
                            uint3 gid [[thread_position_in_grid]])
{
    device PageCurlConstants& constants = *(arguments.constants);
    device PageCurlParticle* particles = arguments.particles;

    if (gid.x == 0 ||
        gid.x >= constants.particleGrid.x ||
        gid.y >= constants.particleGrid.y ||
        gid.z >= 1)
    {
        return;
    }

    const uint index = (gid.y * constants.particleGrid.x) + gid.x;
    device auto& particle = particles[index];

    switch (constants.moveInfo.state)
    {
    case PAGECURL_MOVESTATE_INIT:
    {
        const float2 diff = particle.position.xy - constants.moveInfo.position.xy;
        const float3 moveNormal = cross(float3(diff, 0.0), normalize(constants.moveInfo.tangent));
        particle.restDistance = length(moveNormal);
        particle.positionPrev = particle.position;
        particle.positionMove = float3(diff, 0.45);
        break;
    }
    case PAGECURL_MOVESTATE_MOVE:
    {
        const float3 positionPrev = particle.position;
        const float3 diff = particle.position - float3(constants.moveInfo.position.xy, 0.45);
        const float3 acceleration = float3(0.0, 0.0, -9.81) * 0.01 +
                                    (particle.positionMove - diff) * pow(max(0.0, 1.0 - particle.restDistance), 1.0);
        particle.position = particle.position + (particle.position - particle.positionPrev) * 0.5 + acceleration * 0.1;
        particle.positionPrev = positionPrev;
        break;
    }
    case PAGECURL_MOVESTATE_REST:
        constants.moveInfo.state = PAGECURL_MOVESTATE_NONE;
    default:
    break;
    }
}

kernel void ConstrainParticle(constant PageCurlArgumentBuffer& arguments [[buffer(0)]],
                              uint3 gid [[thread_position_in_grid]],
                              uint3 tid [[threads_per_threadgroup]])
{
    if (gid.z >= 1)
    {
        return;
    }

    device PageCurlConstants& constants = *(arguments.constants);
    device PageCurlParticle* particles = arguments.particles;

    const uint width = constants.particleGrid.x;
    const uint height = constants.particleGrid.y;

    const float restDistanceX = 1.0 / (width - 1);
    const float restDistanceY = 2.0 / (constants.particleGrid.y - 1);
    const float restDistanceXY = sqrt(restDistanceX * restDistanceX + restDistanceY* restDistanceY);

    // Horizontal constraints
    uint2 scan = gid.xy;
    while (scan.y < height)
    {
        scan.x = gid.x;
        while (scan.x < width)
        {
            const uint index = (scan.y * width) + scan.x;
            device auto& particleTopLeft = particles[index];
            if (scan.x < width - 1)
            {
                device auto& particleTopRight = particles[index + 1];
                const float3 diff = particleTopLeft.position - particleTopRight.position;
                const float3 coeff = normalize(diff) * (restDistanceX - length(diff)) * 0.5;
                particleTopRight.position -= coeff;
                particleTopLeft.position += coeff * clamp(float(scan.x), 0.0, 1.0);
            }
            if (scan.x < width - 2)
            {
                device auto& particleTopRight = particles[index + 2];
                const float3 diff = particleTopLeft.position - particleTopRight.position;
                const float3 coeff = normalize(diff) * (2.0 * restDistanceX - length(diff)) * 0.5;
                particleTopRight.position -= coeff;
                particleTopLeft.position += coeff * clamp(float(scan.x), 0.0, 1.0);
            }
            scan.x += tid.x;
        }
        scan.y += tid.y;
    }

    // Vertical constraints
    threadgroup_barrier(mem_flags::mem_device);
    scan.y = gid.y;
    while (scan.y < height)
    {
        scan.x = gid.x;
        while (scan.x < width)
        {
            const uint index = (scan.y * width) + scan.x;
            device auto& particleTopLeft = particles[index];
            if (scan.y < height - 1)
            {
                device auto& particleBottomLeft = particles[index + width];
                const float3 diff = particleBottomLeft.position - particleTopLeft.position;
                const float3 coeff = normalize(diff) * (restDistanceY - length(diff)) * 0.5;

                particleTopLeft.position -= coeff;
                particleBottomLeft.position += coeff;
            }
            if (scan.y < height - 2)
            {
                device auto& particleBottomLeft = particles[index + 2 * width];
                const float3 diff = particleBottomLeft.position - particleTopLeft.position;
                const float3 coeff = normalize(diff) * (2.0 * restDistanceY - length(diff)) * 0.5;

                particleTopLeft.position -= coeff;
                particleBottomLeft.position += coeff;
            }
            scan.x += tid.x;
        }
        scan.y += tid.y;
    }

    // Diagonal constraints
    threadgroup_barrier(mem_flags::mem_device);
    scan.y = gid.y;
    while (scan.y < height)
    {
        scan.x = gid.x;
        while (scan.x < width)
        {
            const uint index = (scan.y * width) + scan.x;
            device auto& particleTopLeft = particles[index];
            if (scan.x > 0 && scan.x < width - 1 && scan.y < height - 1)
            {
                device auto& particleBottomRight = particles[index + width + 1];
                const float3 diff = particleBottomRight.position - particleTopLeft.position;
                const float3 coeff = normalize(diff) * (restDistanceXY - length(diff)) * 0.5;

                particleTopLeft.position -= coeff;
                particleBottomRight.position += coeff;
            }
            scan.x += tid.x;
        }
        scan.y += tid.y;
    }

    // Depth constraint
    threadgroup_barrier(mem_flags::mem_device);
    scan.y = gid.y;
    while (scan.y < height)
    {
        scan.x = gid.x;
        while (scan.x < width)
        {
            device auto& particleDepth = particles[(scan.y * width) + scan.x];
            particleDepth.position.z = max(0.0, particleDepth.position.z);
            scan.x += tid.x;
        }
        scan.y += tid.y;
    }
}

kernel void CalcParticleNormal(constant PageCurlArgumentBuffer& arguments [[buffer(0)]],
                               uint3 gid [[thread_position_in_grid]])
{
    device PageCurlConstants& constants = *(arguments.constants);
    device PageCurlParticle* particles = arguments.particles;

    if (gid.x >= constants.particleGrid.x ||
        gid.y >= constants.particleGrid.y ||
        gid.z >= 1)
    {
        return;
    }

    float3 normal = float3(0.0, 0.0, 0.0);
    const uint width = constants.particleGrid.x;
    const int index = (gid.y * width) + gid.x;
    const float3 position = particles[index].position;

    if (gid.y < constants.particleGrid.y - 1)
    {
        const float3 positionTop = particles[index + width].position;

        if (gid.x > 0)
        {
            const float3 positionLeft = particles[index - 1].position;
            const float3 positionTopLeft = particles[index + width - 1].position;
            normal += normalize(cross(positionTop - position,
                                      positionTopLeft - position));
            normal += normalize(cross(positionTopLeft - position,
                                      positionLeft - position));
        }
        if (gid.x < width - 1)
        {
            const float3 positionRight = particles[index + 1].position;
            const float3 positionTopRight = particles[index + width + 1].position;
            normal += normalize(cross(positionRight - position,
                                      positionTopRight - position));
            normal += normalize(cross(positionTopRight - position,
                                      positionTop - position));
        }
    }

    if (gid.y > 0)
    {
        const float3 positionBottom = particles[index - width].position;

        if (gid.x > 0)
        {
            const float3 positionLeft = particles[index - 1].position;
            const float3 positionBottomLeft = particles[index - width - 1].position;
            normal += normalize(cross(positionLeft - position,
                                      positionBottomLeft - position));
            normal += normalize(cross(positionBottomLeft - position,
                                      positionBottom - position));
        }
        if (gid.x < width - 1)
        {
            const float3 positionRight = particles[index + 1].position;
            const float3 positionBottomRight = particles[index - width + 1].position;
            normal += normalize(cross(positionBottom - position,
                                      positionBottomRight - position));
            normal += normalize(cross(positionBottomRight - position,
                                      positionRight - position));
        }
    }

    particles[index].normal = normalize(normal);
}
