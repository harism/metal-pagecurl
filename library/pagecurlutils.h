#import <MetalKit/MetalKit.h>

@interface PageCurlUtils : NSObject

+(MTLSize) threadgroupSize:(id<MTLComputePipelineState>)computePipeline
           threadsSize:(const MTLSize&)threadsSize;

@end
