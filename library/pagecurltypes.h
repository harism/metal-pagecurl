#import <simd/simd.h>

#if defined(__OBJC__)
    #define PAGECURL_CONSTANT
    #define PAGECURL_DEVICE
    #define PAGECURL_ID(value)
#else
    #define PAGECURL_CONSTANT constant
    #define PAGECURL_DEVICE device
    #define PAGECURL_ID(value) [[id(value)]]
    #import <metal_stdlib>
    using namespace metal;
#endif

typedef enum PageCurlMoveState : char
{
    PAGECURL_MOVESTATE_NONE,
    PAGECURL_MOVESTATE_INIT,
    PAGECURL_MOVESTATE_MOVE,
    PAGECURL_MOVESTATE_REST
} PageCurlMoveState;

typedef struct
{
    PageCurlMoveState state;
    vector_float3 position;
    vector_float3 tangent;
} PageCurlMoveInfo;

typedef struct
{
    matrix_float4x4 projectionMatrix;
    vector_uint2 particleGrid;
    unsigned int particleCount;
    unsigned int triangleCount;
    PageCurlMoveInfo moveInfo;
} PageCurlConstants;

typedef struct
{
    vector_float3 position;
    vector_float3 positionMove;
    vector_float3 positionPrev;
    vector_float3 normal;
    vector_float2 texturePosition;
    float restDistance;
} PageCurlParticle;

typedef struct
{
    vector_int3 particleIndices;
} PageCurlTriangle;

typedef enum PageCurlArgumentIndex : int
{
    PAGECURL_ARGUMENT_IMAGE,
    PAGECURL_ARGUMENT_PARTICLES,
    PAGECURL_ARGUMENT_TRIANGLES,
    PAGECURL_ARGUMENT_CONSTANTS
} PageCurlArgumentIndex;

typedef struct
{
#if defined(__OBJC__)
    void* image;
#else
    texture2d<float, access::sample> image PAGECURL_ID(PAGECURL_ARGUMENT_IMAGE);
#endif
    PAGECURL_DEVICE PageCurlParticle* particles PAGECURL_ID(PAGECURL_ARGUMENT_PARTICLES);
    PAGECURL_DEVICE PageCurlTriangle* triangles PAGECURL_ID(PAGECURL_ARGUMENT_TRIANGLES);
    PAGECURL_DEVICE PageCurlConstants* constants PAGECURL_ID(PAGECURL_ARGUMENT_CONSTANTS);
} PageCurlArgumentBuffer;

#undef PAGECURL_CONSTANT
#undef PAGECURL_DEVICE
#undef PAGECURL_ID

