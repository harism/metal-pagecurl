#import <metal_stdlib>
#import <simd/simd.h>
#import "pagecurltypes.h"

using namespace metal;

kernel void InitializeTriangle(device PageCurlTriangle* triangles [[buffer(0)]],
                               constant PageCurlConstants& constants [[buffer(1)]],
                               uint3 gid [[thread_position_in_grid]])
{
    if (gid.x >= (constants.particleGrid.x - 1) ||
        gid.y >= (constants.particleGrid.y - 1) ||
        gid.z >= 2)
    {
        return;
    }

    int3 triangle;
    triangle.x = (gid.y * constants.particleGrid.x) + gid.x + gid.z;
    triangle.y = ((gid.y + 1) * constants.particleGrid.x) + gid.x;
    triangle.z = ((triangle.x + 1) * (1 - gid.z)) + ((triangle.y + 1) * gid.z);

    const uint triangleIndex = 2 * ((gid.y * (constants.particleGrid.x - 1)) + gid.x) + gid.z;
    triangles[triangleIndex].particleIndices = triangle;
}

struct VertexData
{
    float4 position [[position]];
    float2 texturePosition;
    float4 normal;
};

vertex VertexData
RenderTriangleVertex(constant PageCurlArgumentBuffer& arguments [[buffer(0)]],
                     uint instanceId [[instance_id]],
                     uint vertexId [[vertex_id]])
{
    VertexData out;
    const uint particleIndex = arguments.triangles[instanceId].particleIndices[vertexId];
    const PageCurlParticle particle = arguments.particles[particleIndex];
    out.position = arguments.constants->projectionMatrix * float4(particle.position, 1.0);
    out.texturePosition = particle.texturePosition;
    out.normal = float4(particle.normal, 0.0);
    return out;
}

fragment float4
RenderTriangleFragment(VertexData in [[stage_in]],
                       constant PageCurlArgumentBuffer& arguments [[buffer(0)]])
{
    constexpr sampler textureSampler (mag_filter::linear,
                                      min_filter::linear);
    const float3 color = arguments.image.sample(textureSampler, in.texturePosition).rgb;
    return float4(color * (0.15 + abs(normalize(in.normal).z) * 0.85), 1.0);
}

struct PointData
{
    float4 position [[position]];
    float pointSize [[point_size]];
    float distance;
};

vertex PointData
RenderPointVertex(constant PageCurlArgumentBuffer& arguments [[buffer(0)]],
                  uint vertexId [[vertex_id]])
{
    PointData out;
    const float3 particlePosition = arguments.particles[vertexId].position;
    out.position = arguments.constants->projectionMatrix * float4(particlePosition, 1.0);
    out.pointSize = 2.0;
    out.distance = 1.0 - arguments.particles[vertexId].restDistance;
    return out;
}

fragment float4 RenderPointFragment(PointData in [[stage_in]])
{
    return in.distance * float4(0.35, 0.45, 1.0, 1.0);
}

struct MovePositionData
{
    float4 color;
    float4 position [[position]];
    float pointSize [[point_size]];
};

vertex MovePositionData
RenderMovePositionVertex(constant PageCurlConstants& constants [[buffer(0)]],
                         uint vertexId [[vertex_id]])
{
    MovePositionData out;
    out.color = float4(0.35, 0.45, 1.0, 1.0);
    out.position = constants.projectionMatrix * float4(constants.moveInfo.position, 1.0);
    out.pointSize = 4.0;
    return out;
}

fragment float4 RenderMovePositionFragment(MovePositionData in [[stage_in]])
{
    return in.color;
}

struct MoveLineData
{
    float4 position [[position]];
    float4 color;
};

vertex MoveLineData
RenderMoveLineVertex(constant PageCurlConstants& constants [[buffer(0)]],
                     uint vertexId [[vertex_id]])
{
    MoveLineData out;
    out.position = float4(constants.moveInfo.position, 1.0);

    if (vertexId > 0)
    {
        out.position.xyz += constants.moveInfo.tangent;
    }
    
    out.position = constants.projectionMatrix * out.position;
    out.color = float4(0.0, 0.0, 0.75, 1.0);
    return out;
}

fragment float4 RenderMoveLineFragment(MoveLineData in [[stage_in]])
{
    return in.color;
}
