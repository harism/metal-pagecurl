#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>
#import "pagecurlview.h"
#import "pagecurlviewdelegate.h"

@implementation PageCurlView
{
    PageCurlViewDelegate* _pageCurlViewDelegate;
}

-(instancetype)initWithParticleGrid:(const PageCurlParticleGrid&)particleGrid
               frameRect:(const CGRect&)frameRect
{
    id<MTLDevice> device = MTLCreateSystemDefaultDevice();
    self = [self initWithParticleGrid:particleGrid frameRect:frameRect device:device];
    [device release];
    return self;
}

-(instancetype)initWithParticleGrid:(const PageCurlParticleGrid&)particleGrid
               frameRect:(const CGRect&)frameRect
               device:(id<MTLDevice>)device
{
    if (particleGrid._width == 0 || particleGrid._height == 0)
    {
        return nil;
    }

    if (self = [super initWithFrame:frameRect device:device])
    {
        _pageCurlViewDelegate = [[PageCurlViewDelegate alloc] initWithPageCurlView:self
                                                              particleGrid:particleGrid];
        [self setDelegate:_pageCurlViewDelegate];
    }
    return self;
}

-(void)dealloc
{
    [_pageCurlViewDelegate release];
    [super dealloc];
}

-(BOOL)mouseDownCanMoveWindow
{
    return NO;
}

-(void)mouseDown:(NSEvent*)event
{
    [_pageCurlViewDelegate mouseDown:self event:event];
}

@end
