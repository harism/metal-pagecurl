#import <MetalKit/MetalKit.h>
#import "pagecurlview.h"

@interface PageCurlViewDelegate : NSObject<MTKViewDelegate>

-(nullable instancetype)initWithPageCurlView:(nonnull PageCurlView*)pageCurlView
                        particleGrid:(const PageCurlParticleGrid&)particleGrid;

-(void)mtkView:(nonnull MTKView*)view drawableSizeWillChange:(CGSize)size;

-(void)drawInMTKView:(nonnull MTKView*)view;

-(void)mouseDown:(nonnull MTKView*)view
       event:(nonnull NSEvent*)event;

@end
