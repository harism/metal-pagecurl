#import <simd/simd.h>
#import <MetalKit/MetalKit.h>
#import "pagecurlviewdelegate.h"
#import "pagecurlrenderer.h"
#import "pagecurlsimulation.h"
#import "pagecurltypes.h"

@implementation PageCurlViewDelegate
{
    id<MTLDevice> _device;
    id<MTLLibrary> _library;
    id<MTLCommandQueue> _commandQueue;
    id<MTLBuffer> _constantsBuffer;

    PageCurlConstants _pagecurlConstants;
    PageCurlRenderer* _pagecurlRenderer;
    PageCurlSimulation* _pagecurlSimulation;
}

-(instancetype)initWithPageCurlView:(PageCurlView*)pageCurlView
               particleGrid:(const PageCurlParticleGrid&) particleGrid
{
    if (self = [super init])
    {
        _device = [pageCurlView device];

        NSString* libraryFile = [[NSBundle mainBundle] pathForResource:@"pagecurl" ofType:@"metallib"];
        _library = [_device newLibraryWithFile:libraryFile error:nil];
        _commandQueue = [_device newCommandQueue];

        _pagecurlConstants.projectionMatrix = matrix_identity_float4x4;
        _pagecurlConstants.particleGrid = (vector_uint2) { particleGrid._width, particleGrid._height };
        _pagecurlConstants.particleCount = particleGrid._width * particleGrid._height;
        _pagecurlConstants.triangleCount = 2 * ((particleGrid._width - 1) * (particleGrid._height - 1));
        _pagecurlConstants.moveInfo.state = PageCurlMoveState::PAGECURL_MOVESTATE_NONE;

        _constantsBuffer = [_device newBufferWithLength:sizeof(PageCurlConstants)
                                    options:MTLResourceStorageModeManaged];

        _pagecurlSimulation = [[PageCurlSimulation alloc] initWithLibrary:_library
                                                          constants:_pagecurlConstants
                                                          constantsBuffer:_constantsBuffer];
        _pagecurlRenderer = [[PageCurlRenderer alloc] initWithView:pageCurlView
                                                      library:_library
                                                      constants:_pagecurlConstants
                                                      constantsBuffer:_constantsBuffer
                                                      particleBuffer:[_pagecurlSimulation particleBuffer]];
    }
    return self;
}

-(void)dealloc
{
    [_device release];
    [_library release];
    [_commandQueue release];
    [_constantsBuffer release];
    [_pagecurlSimulation release];
    [_pagecurlRenderer release];
    [super dealloc];
}

-(void)mtkView:(MTKView*)view drawableSizeWillChange:(CGSize)size
{
    const float dx = 1.05;
    const float dy = 1.10;

    matrix_float4x4 adjustMetalProjection = matrix_from_rows(
        (vector_float4) { 1, 0, 0, 0 },
        (vector_float4) { 0, 1, 0, 0 },
        (vector_float4) { 0, 0, 0.5, 0.5 },
        (vector_float4) { 0, 0, 0, 1 }
    );

    matrix_float4x4 orthoProjectionOpenGL = matrix_from_rows(
        (vector_float4) { 1 / dx, 0, 0, 0 },
        (vector_float4) { 0, 1 / dy, 0, 0 },
        (vector_float4) { 0, 0, -1, 0 },
        (vector_float4) { 0, 0, 0, 1 }
    );

    _pagecurlConstants.projectionMatrix = matrix_multiply(adjustMetalProjection, orthoProjectionOpenGL);
}

-(void)drawInMTKView:(nonnull MTKView*)view
{
    PageCurlConstants* constants = (PageCurlConstants*)[_constantsBuffer contents];

    if (constants)
    {
        if (constants->moveInfo.state == PAGECURL_MOVESTATE_NONE &&
            _pagecurlConstants.moveInfo.state == PAGECURL_MOVESTATE_INIT)
        {
            constants->moveInfo.state = PAGECURL_MOVESTATE_INIT;
        }
        else if (constants->moveInfo.state == PAGECURL_MOVESTATE_INIT &&
                 _pagecurlConstants.moveInfo.state == PAGECURL_MOVESTATE_INIT)
        {
            constants->moveInfo.state = PAGECURL_MOVESTATE_MOVE;
            _pagecurlConstants.moveInfo.state = PAGECURL_MOVESTATE_MOVE;
        }
        else if (_pagecurlConstants.moveInfo.state == PAGECURL_MOVESTATE_REST)
        {
            constants->moveInfo.state = PAGECURL_MOVESTATE_REST;
        }
        else
        {
            _pagecurlConstants.moveInfo.state = constants->moveInfo.state;
        }

        constants->moveInfo.position = _pagecurlConstants.moveInfo.position;
        constants->moveInfo.tangent = _pagecurlConstants.moveInfo.tangent;
        constants->particleCount = _pagecurlConstants.particleCount;
        constants->particleGrid = _pagecurlConstants.particleGrid;
        constants->projectionMatrix = _pagecurlConstants.projectionMatrix;
        constants->triangleCount = _pagecurlConstants.triangleCount;

        [_constantsBuffer didModifyRange:NSMakeRange(0, sizeof(PageCurlConstants))];
    }

    [_pagecurlSimulation simulate:_commandQueue
                         constants:_pagecurlConstants];

    [_pagecurlRenderer render:view
                       commandQueue:_commandQueue
                       constants:_pagecurlConstants];
}

-(vector_float3)eventPositionInView:(MTKView*)view
                event:(NSEvent*)event
{
    NSPoint viewPosition = [view convertPoint:[event locationInWindow] fromView:nil];
    vector_float4 eventPosition = (vector_float4) {
            ((viewPosition.x / [view drawableSize].width) * 2.0) - 1.0,
            ((viewPosition.y / [view drawableSize].height) * 2.0) - 1.0,
            0.0,
            1.0 };
    matrix_float4x4 projectionMatrixInv = matrix_invert(_pagecurlConstants.projectionMatrix);
    eventPosition = matrix_multiply(projectionMatrixInv, eventPosition);
    return (vector_float3) { eventPosition.x, eventPosition.y, eventPosition.z };
}

-(void)mouseDown:(MTKView*)view
       event:(NSEvent*)event
{
    BOOL isTrackingDone = NO;
    _pagecurlConstants.moveInfo.state = PAGECURL_MOVESTATE_INIT;
    vector_float3 position = _pagecurlConstants.moveInfo.position = [self eventPositionInView:view
                                                                          event:event];

    if (position.x < 0.0)
    {
        const float diffX = -1.0 - position.x;
        vector_float2 top = vector_normalize((vector_float2){ diffX, 1.0 - position.y });
        vector_float2 middle = vector_normalize((vector_float2){ diffX, 0.0 - position.y });
        vector_float2 bottom = vector_normalize((vector_float2){ diffX, -1.0 - position.y });
        _pagecurlConstants.moveInfo.tangent.y = (top.x + middle.x + bottom.x) / 3.0;
        _pagecurlConstants.moveInfo.tangent.x = (top.y + middle.y + bottom.y) / 3.0;
        _pagecurlConstants.moveInfo.tangent.z = 0.0;
    }
    else
    {
        const float diffX = 1.0 - position.x;
        vector_float2 top = vector_normalize((vector_float2){ diffX, 1.0 - position.y });
        vector_float2 middle = vector_normalize((vector_float2){ diffX, 0.0 -position.y });
        vector_float2 bottom = vector_normalize((vector_float2){ diffX, -1.0 - position.y });
        _pagecurlConstants.moveInfo.tangent.y = (top.x + middle.x + bottom.x) / 3.0;
        _pagecurlConstants.moveInfo.tangent.x = (top.y + middle.y + bottom.y) / 3.0;
        _pagecurlConstants.moveInfo.tangent.z = 0.0;
    }

    while (isTrackingDone == NO)
    {
        event = [[view window] nextEventMatchingMask:NSEventMaskLeftMouseDragged |
                                                     NSEventMaskLeftMouseUp];
        switch ([event type])
        {
        case NSEventTypeLeftMouseDragged:
            _pagecurlConstants.moveInfo.position = [self eventPositionInView:view
                                                         event:event];
            break;
        case NSEventTypeLeftMouseUp:
            isTrackingDone = YES;
            _pagecurlConstants.moveInfo.state = PAGECURL_MOVESTATE_REST;
            break;
        default:
            break;
        }
    }
}

@end
