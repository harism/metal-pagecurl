#import <MetalKit/MetalKit.h>

struct PageCurlParticleGrid
{
    PageCurlParticleGrid(uint width, uint height)
    {
        _width = width;
        _height = height;
    }
    uint _width;
    uint _height;
};

@interface PageCurlView : MTKView

-(nullable instancetype)initWithParticleGrid:(const PageCurlParticleGrid&)particleGrid
                        frameRect:(const CGRect&)frameRect;

-(nullable instancetype)initWithParticleGrid:(const PageCurlParticleGrid&)particleGrid
                        frameRect:(const CGRect&)frameRect
                        device:(nonnull id<MTLDevice>)device;

@end
