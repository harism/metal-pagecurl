#import <MetalKit/MetalKit.h>
#import "pagecurltypes.h"

@interface PageCurlRenderer : NSObject

-(nullable instancetype) initWithView:(nonnull MTKView*)view
                         library:(nonnull id<MTLLibrary>)library
                         constants:(const PageCurlConstants&)constants
                         constantsBuffer:(nonnull id<MTLBuffer>)constantsBuffer
                         particleBuffer:(nonnull id<MTLBuffer>)particleBuffer;

-(void) render:(nonnull MTKView*)view
        commandQueue:(nonnull id<MTLCommandQueue>)commandQueue
        constants:(const PageCurlConstants&)constants;

@end
